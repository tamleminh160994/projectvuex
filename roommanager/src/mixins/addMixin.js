import { mapActions } from "vuex";
export const addMixins = {
  data() {
    return {
      newRoom: ""
    };
  },
  methods: {
    ...mapActions(["addNewRoom"]),
    addRoom() {
      const newRoom = {
        title: this.newRoom,
        available: true
      };
      this.addNewRoom(newRoom);
      this.newRoom = "";
    }
  }
};
