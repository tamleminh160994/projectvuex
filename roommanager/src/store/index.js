import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    Rooms: []
  },
  mutations: {
    LOAD_DATAROOMS(state, newRooms) {
      state.Rooms = newRooms;
    },
    ADD_NEWROOM(state, newRoom) {
      //state.Rooms = [newRoom, ...state.Rooms];
      state.Rooms.push(newRoom);
    },
    UPDATE_STATUSROOM(state, { available, index }) {
      state.Rooms[index].available = available;
    },
    REMOVE_SELECTEDROOM(state, RoomId) {
      state.Rooms = state.Rooms.filter(Room => Room.id !== RoomId);
    }
  },
  actions: {
    loadDataRooms({ commit }) {
      axios
        .get("http://localhost:3000/Rooms")
        .then(response => {
          commit("LOAD_DATAROOMS", response.data);
        })
        .catch(error => {
          console.log(error);
        });
    },
    addNewRoom({ commit }, newRoom) {
      axios
        .post("http://localhost:3000/Rooms", newRoom)
        .then(response => {
          commit("ADD_NEWROOM", response.data);
        })
        .catch(error => {
          console.log(error);
        });
    },
    toggleAvailableRoom({ commit, state }, index) {
      var id = state.Rooms[index].id;
      const updatedRoom = Object.assign({}, state.Rooms[index]);
      updatedRoom.available = !updatedRoom.available;
      axios
        .put(`http://localhost:3000/Rooms/${id}`, updatedRoom)
        .then(response => {
          var updatedRoom = response.data;
          var available = updatedRoom.available;
          commit("UPDATE_STATUSROOM", { available, index });
        });
    },
    removeSelectedRoom({ commit }, RoomId) {
      axios
        .delete(`http://localhost:3000/Rooms/${RoomId}`)
        .then(() => {
          commit("REMOVE_SELECTEDROOM", RoomId);
        })
        .catch(error => {
          console.log(error);
        });
    }
  },
  getters: {
    roomLength: state => {
      return state.Rooms.length;
    },
    availableRoom: state => {
      return state.Rooms.filter(Room => Room.available).length;
    },
    fullRoom: state => {
      return state.Rooms.filter(Room => !Room.available).length;
    }
  },
  modules: {}
});
//Finish VUEX roomanager
